# CSS and .JS free

## To-Do

- Remove Markdown .CSS / .JS - Make all info horizontale
- Add Toggle Detail Button
- Add Info buttons
- CSS prettify 

## Let's use some Combinators

- https://developer.mozilla.org/en-US/docs/Web/CSS/General_sibling_combinator
- https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors

- https://techbrij.com/css-selector-adjacent-child-sibling
- http://jsfiddle.net/RpfLa/
- https://techbrij.com/css-selector-adjacent-child-sibling

## Guide

- `base.html` for css
- `row_num.html` as test page
- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/label#Attributes

## Code Snippets

Use `for` to make pseude buttons for check boxes & radio buttons
```
<button class="details_toggle"><label for="toggle_checkbox">Toggle Details</label></button>
```

So with
```
    div#extra_details{
        display: none;
    }
    
    :checked ~ div#extra_details
    {
        display: block;
    }
```

and `.html`
```
    <button class="details_toggle"><label for="toggle_checkbox">Toggle Details</label></button>
    <input id="toggle_checkbox" type="checkbox" />
    <div id="extra_details">
    ...
    </div>
```


# ppideas

Execute the following cell in Jupyter Labs as Proof-of-Concepts to use CSS for show/hide button functionality


```
from IPython.core.display import display, HTML
display(HTML('''<style>
input,
ul {
    display: none;
}

button:active + ul,
:checked + ul {
    display: block;
}
</style>
<button>hold down here</button>
<ul>
    <li>Foo</li>
    <li>Bar</li>
    <li>Baz</li>
</ul>

<br />
<br />

<button><label for="cb1">click here</label></button>
<input id="cb1" type="checkbox" />
<ul>
    <li>Foo</li>
    <li>Bar</li>
    <li>Baz</li>
</ul>'''))
```


## Output GIF

![output gif](https://media.giphy.com/media/StXGbum8yj3uxf4CyC/giphy.gif)